

import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Test11Component } from './test11/test11.component';
import { Test1Component } from '../my-module/test1/test1.component';
import { Routes, RouterModule } from '@angular/router';
import { Test2Component } from './test2/test2.component';

const appRoutes: Routes = [
  { path: '', component : Test11Component},
  {path:'',component:Test2Component}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes)
  ],

  declarations: [Test2Component,Test11Component]
})

export class MyModule1Module { }


