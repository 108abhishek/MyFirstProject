import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Testj1Component } from './testj1.component';

describe('Testj1Component', () => {
  let component: Testj1Component;
  let fixture: ComponentFixture<Testj1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Testj1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Testj1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
