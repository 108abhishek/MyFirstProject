import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Testj1Component } from './testj1/testj1.component';
import { Ch1Component } from './ch1/ch1.component';
import { Ch2Component } from './ch2/ch2.component';

const appRoutes: Routes = [
  { path: '', component : Testj1Component},
  
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes)
  ],
  declarations: [Testj1Component, Ch1Component, Ch2Component]
})
export class Jagdish1Module { }
