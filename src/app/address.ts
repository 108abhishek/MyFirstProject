/*It is the data model class
? means it is optional
*/
export class Address {
    constructor(
        public firstname?: string,
        public lastname?: string,
        public address?: string,
        public city?: string,
        public state?: string,
        public postal?: string
    ){}
}