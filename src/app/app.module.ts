import { Button1Component } from './button/button1/button1.component';
import { ClickMeComponent } from './click-me/click-me.component';
import {MatRadioModule} from '@angular/material';
import { CPopUpComponent } from './pop-up/c-pop-up/c-pop-up.component';
import { MyFormComponent } from './my-form/my-form.component';
import { FormComponent } from './form-control/form/form.component';
import { FormControlModule } from './form-control/form-control.module';
import { Child1Component } from './my-module/childs/child1/child1.component';
import { Child2Component } from './my-module/childs/child2/child2.component';
import { HeroFormComponent } from '../class-file/hero-form.component';
import { FormsModule } from '@angular/forms';
import {FormGroup,FormControl} from '@angular/forms';
import { MyModuleModule } from './my-module/my-module.module';
import { MyModule1Module } from './my-module1/my-module1.module';
import { Test11Component } from './my-module1/test11/test11.component';
import { Test1Component } from './my-module/test1/test1.component';
import { BrowserModule } from '@angular/platform-browser';
import { Component, NgModule } from '@angular/core';
import {MatMenuModule} from '@angular/material';
import {MatSidenavModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {MatButtonModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {MatTableModule} from '@angular/material';
import {MatPaginatorModule} from '@angular/material';
import {MatSortModule} from '@angular/material';
import {MatCardModule} from '@angular/material';
import {MatListModule} from '@angular/material';
import {MatGridListModule} from '@angular/material';
import {MatTabsModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material';
import {MatTooltipModule} from '@angular/material';
import {MatSnackBarModule} from '@angular/material';
import {MatDialogModule} from '@angular/material';
import {MatButtonToggleModule} from '@angular/material';
import {MatChipsModule} from '@angular/material';
import {MatIconModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material';
import {MatProgressBarModule} from '@angular/material';
import { MdInputModule, MdButtonModule } from '@angular/material';
import { LoadChildren } from '@angular/router';
import {AboutUsComponent} from './about-us/about-us.component';
import {MdDatepickerModule} from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { Ch2Component } from "./jagdish1/ch2/ch2.component";
import { RadioControlValueAccessor } from '@angular/forms';




const appRoutes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: 'app-test1',       
  component:Test1Component,
  children:[{path:'app-child1',component:Child1Component, }]
  },
  { path: 'app-test1',       
  component:Test1Component,
  children:[{path:'app-child2',component:Child2Component, }]
  },
  { path: 'app-test1',       
  component:Test1Component,
  children:[{path:'hero-form',component:HeroFormComponent, }]
  },
  
  { path: 'test1', loadChildren:'app/my-module/my-module.module#MyModuleModule'},
  { path: 'test2', loadChildren:'app/my-module1/my-module1.module#MyModule1Module'},
  {path: 'my-module1',    loadChildren:'app/my-module1/my-module1.module#MyModule1Module'},
  { path: 'my-module',    loadChildren:'app/my-module/my-module.module#MyModuleModule' },
  { path: 'app-test11',    loadChildren:'app/my-module1/my-module1.module#MyModule1Module'},
  { path: 'app-test1',    loadChildren:'app/my-module/my-module.module#MyModuleModule' },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'app-button1',    loadChildren:'app/button1/button1.button#Button1Module' },

  {path: 'app-test1', 
  component:Test1Component,
  children:[{path:'app-child1', component:Child1Component,}]
  },
  {path: 'app-test1',
  component:Test1Component,
  children:[{path:'app-child2', component:Child2Component,}]
  },
  {path: 'hero-form',component:HeroFormComponent,},

  {path: 'button1',component: AppComponent,},
  {path: 'popUps',component:CPopUpComponent,},
 // {path: 'p=opUps',component:CPopUpComponent,},
 // {path: 'button1',component:Button1Component,},
  

];


@NgModule({
 
  declarations: [
    Button1Component,
    AppComponent,
    AboutUsComponent, 
    Test1Component,
    Child1Component,
    Child2Component,
    Test1Component,
    Child1Component,
    Child2Component,
    HeroFormComponent,  
    MyFormComponent,
    
    CPopUpComponent,
    ClickMeComponent
    
  ],

  imports: [
    
    BrowserModule,MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCheckboxModule,MatCardModule,MatListModule,
    MatGridListModule,MatTabsModule,MatExpansionModule,
    BrowserModule,
    MatCheckboxModule,
    MatCardModule,
    MatListModule,
    MatGridListModule,
    MatTabsModule,
    MatExpansionModule,
    MatSnackBarModule,
    BrowserModule,
    MatCheckboxModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatTooltipModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserModule,
    MatCheckboxModule,
    MatButtonModule,
    MatDialogModule,
    MatTooltipModule,
    MatCardModule,
    MatListModule,
    MatGridListModule,
    MatTabsModule,
    MatExpansionModule,
    MatChipsModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MdInputModule, 
    MdButtonModule,
    FormControlModule,
    FormsModule,
    
    
    RouterModule.forRoot(
      appRoutes
    )

  ],

   
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


