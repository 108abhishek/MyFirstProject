import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CPopUpComponent } from './c-pop-up/c-pop-up.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CPopUpComponent]
})
export class PopUpModule { }
