import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CPopUpComponent } from './c-pop-up.component';

describe('CPopUpComponent', () => {
  let component: CPopUpComponent;
  let fixture: ComponentFixture<CPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
