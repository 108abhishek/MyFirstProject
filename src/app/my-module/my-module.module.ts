

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Test1Component } from './test1/test1.component';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { ChildComponent } from './child/child.component';
import { Child1Component } from './child/child1/child1.component';
import { Child2Component } from './child/child2/child2.component';
import { Test2Component } from "../my-module1/test2/test2.component";

const appRoutes: Routes = [
  { path: '', component : Test1Component},
  { path: '', component : Test2Component},

]



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes)
  ],


  declarations: 
  [Test1Component, ChildComponent, Child1Component, Child2Component]


})
export class MyModuleModule { }
