import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child1',
  templateUrl: './child1.component.html',
  styleUrls: ['./child1.component.scss']
})
export class Child1Component implements OnInit {

  constructor() { }

  clickMessage = 'hello';
  onClickMe() {
    this.clickMessage = 'You are my hero!';
  }

  values = '';
  
    onKey(event: any) { // without type info
      this.values += event.target.values + ' | ';
    }

    value1 = '';
    onEnter(value1: string) { this.value1 = value1; }
  ngOnInit() {
  }

}
